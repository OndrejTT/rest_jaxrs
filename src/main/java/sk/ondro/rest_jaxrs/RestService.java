package sk.ondro.rest_jaxrs;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.logging.Logger;


@Path("/RestService")
public class RestService {

    private final static Logger logger = Logger.getLogger(RestService.class.getName());

    @GET
    @Path("getHelloMessage")
    public String getHelloMessage() {
        logger.info("getHelloMessage()");
        return "Hello REST world!";
    }

    @GET
    @Path("/echo")
    public Response getEchoMessage(@QueryParam("message") String message) {
        return Response.ok("Your message was: " + message).build();
    }

    @GET
    @Path("/object")
    //Need to put
    //Accept    application/xml or application/json
    //nto header of request
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public SimpleObject getObject() {
        return new SimpleObject(1, "Test");
    }
}