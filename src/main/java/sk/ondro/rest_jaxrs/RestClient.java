package sk.ondro.rest_jaxrs;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

//thanks to this I will have access to call() ... most likely ... ???
@ApplicationScoped
@ManagedBean
public class RestClient {

    public void call() {
        Client client = ClientBuilder.newClient();
        SimpleObject o = client.target("http://localhost:8080/rest_jaxrs/api/RestService/object").request().get(SimpleObject.class);
        System.out.println(o);
    }
}